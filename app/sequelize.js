var Sequelize = require('sequelize');
// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'password1234';


var sequelize = new Sequelize('sequelize1', MYSQL_USERNAME, MYSQL_PASSWORD,
    {
        logging: console.log,
         pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);
sequelize
.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});
module.exports = sequelize