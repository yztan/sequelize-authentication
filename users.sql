CREATE TABLE `users` (
  `id` int(11)                          NOT NULL AUTO_INCREMENT,
  `username` varchar(255)               NOT NULL,
  `password` varchar(255)               DEFAULT NULL,
  `salt` varchar(255)              DEFAULT NULL,
  `updatedAt` varchar(255)               DEFAULT NULL,
  `createdAt` varchar(255)                  NOT NULL,
  PRIMARY KEY (`id`)
);